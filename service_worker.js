var cacheName = 'ccscs2020-v1';

var filesToCache = [
  './',
  './index.html',
  './css/CCSCS.css',
  './js/CCSCS.js',
  './img/header.png',
  './img/ie20.png',
  './img/horz.orangered.red.png',
  './img/icons/android-icon-192x192.png',
  './img/icons/android-icon-512x512.png'
];


async function cacheFirst(req) {
  const cache = await caches.open(cacheName);
  const cachedResponse = await cache.match(req);
  return cachedResponse || networkFirst(req);
}

async function networkFirst(req) {
  const cache = await caches.open(cacheName);
  try { 
    const fresh = await fetch(req);
    cache.put(req, fresh.clone());
    return fresh;
  } catch (e) { 
    const cachedResponse = await cache.match(req);
    return cachedResponse;
  }
}


/* Start the service worker and cache all of the app's content */
self.addEventListener('install', async event => {
  const cache = await caches.open(cacheName); 
  await cache.addAll(staticAssets);
});

/* Serve cached content when offline */
self.addEventListener('fetch', event => {
  const req = event.request;
  event.respondWith(networkFirst(req));
});